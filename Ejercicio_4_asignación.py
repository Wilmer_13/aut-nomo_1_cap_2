# Calcular_el_valor_y_el_tipo_de_la_expresión
# Author_Wilmer_Cruz
# Email_wilmer.cruz@unl.edu.ec

alto = 12.0
ancho = 17
print ("ancho= 17")
print ("alto= 12.0")
print ("operación uno: ancho/2")
operación_uno = ancho/2
print("El resultado de la operación uno es: ",operación_uno)
print (type(operación_uno))
print ("operación dos: ancho/2.0")
operación_dos = ancho/2.0
print("El resultado de la operación dos es: ",operación_dos)
print (type(operación_dos))
print ("operación tres: alto/3")
operación_tres = alto/3
print ("El resultado de la operación tres es: ",operación_tres)
print (type(operación_tres))
print ("operación cuatro: 1+2+5")
operación_cuatro = 1+2+5
print ("El resultado de la operación cuatro es: ",operación_cuatro)
print (type (operación_cuatro))